export default {
    s3: {
      REGION: "ap-southeast-1",
      BUCKET: "notes-app-uploads-94"
    },
    apiGateway: {
      REGION: "ap-southeast-1",
      URL: "https://qw7hujs3s7.execute-api.ap-southeast-1.amazonaws.com/prod"
    },
    cognito: {
      REGION: "YOUR_COGNITO_REGION",
      USER_POOL_ID: "YOUR_COGNITO_USER_POOL_ID",
      APP_CLIENT_ID: "YOUR_COGNITO_APP_CLIENT_ID",
      IDENTITY_POOL_ID: "YOUR_IDENTITY_POOL_ID"
    }
  };